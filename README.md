
<img src="https://i.ibb.co/52MwPcT/robart-Mohammad-300.png" alt="robart-Mohammad-300" border="0">

<p>
Hello,

1 - CoreData is implemented into the project, So if you enter a formula and get success response, the formula and the image will save in CoreData. If you enter the same formula again, there is no need to access the internet, and the image of the formula load immediately from CoreData.

2 - To test the project we can use the Unit test. for this project, there is no unit test implementation right now. To see my unit test sample, please go to my other project if you need:
https://gitlab.com/sahelbook/mt-calculatedistance

Thank you.
</p>