//
//  APIsList.swift
//  Peyk-Swift
//
//  Created by mohammad takbiri on 11/5/19.
//  Copyright © 2019 mohammad takbiri. All rights reserved.
//

import Foundation

class APIsList {
    
    private init() {}
    
    static var mainBaseUrl = "https://en.wikipedia.org/api/rest_v1/media/math/"
    
    
    
    
    static func getHashCode(formula: String ,complitaion: @escaping([String:Any],Bool,[AnyHashable:Any]) -> Void) {
        
        guard let url = URL(string: "\(mainBaseUrl)check/inline-tex") else { return }
        
        let parameter = ["q": formula]
        
        POSTNetworking.getData(withUrl: url, withParameter: parameter) { (response,success,headerFieleds) in
            complitaion(response,success,headerFieleds)
        }
        
    }
    
}
