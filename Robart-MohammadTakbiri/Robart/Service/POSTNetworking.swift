//
//  POSTNetworking.swift
//  Peyk-Swift
//
//  Created by mohammad takbiri on 11/4/19.
//  Copyright © 2019 mohammad takbiri. All rights reserved.
//

import UIKit

class POSTNetworking {
    
    private init() {}
    
    static func getData(withUrl url:URL, withParameter parameter:[String: Any], complitaion: @escaping ([String:Any],Bool, [AnyHashable:Any])->Void){
        
        NetworkingService.shared.callAPI(fromURL: url, withParamter: parameter) { (json, headerFields) in
            
            let response = GetPOSTResponse(json: json)
            
            let json = response.json
            
            if json["success"] != nil {
                complitaion(json, true, headerFields)
            }else {
                complitaion(json, false, headerFields)
            }
        }
    }
}
