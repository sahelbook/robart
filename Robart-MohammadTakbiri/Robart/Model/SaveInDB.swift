//
//  SaveInDB.swift
//  Robart-MohammadTakbiri
//
//  Created by mohammad takbiri on 11/15/20.
//

import UIKit
import CoreData

class SaveInDB {
    
    static func save(formulaStr: String, imageData: Data){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let forumla = NSEntityDescription.entity(forEntityName: "Formula", in: context)
        let newFormula = NSManagedObject(entity: forumla!, insertInto: context)
        
        newFormula.setValue(formulaStr, forKey: "formula")
        newFormula.setValue(imageData, forKey: "image")
        
        do {
            try context.save()
            print("data saved")
            
        }catch let error {
            print("something happened: \(error)")
        }

        
    }
    
}
