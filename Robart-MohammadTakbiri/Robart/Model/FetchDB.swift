//
//  FetchDB.swift
//  Robart-MohammadTakbiri
//
//  Created by mohammad takbiri on 11/15/20.
//

import UIKit
import CoreData

class FetchDB {
    
    static func fetch() -> [[String:Any]]{
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Formula")
        
        request.returnsObjectsAsFaults = false
        
        do {
            
            var results = [[String:Any]]()
            
            let result = try context.fetch(request)
            
            for data in result as! [NSManagedObject] {
                print("row is \(data.value(forKey: "formula")!)")
                
                results.append(
                    [
                        "formula":"\(data.value(forKey: "formula")!)",
                        "image":data.value(forKey: "image") as! Data
                    ]
                )
            }
            
            return results
            
        }catch {
            print("oops .. cant fetch !")
        }
        
        return [["":""]]
    }
    
}
