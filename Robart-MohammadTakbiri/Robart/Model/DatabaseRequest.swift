//
//  DatabaseRequest.swift
//  Robart-MohammadTakbiri
//
//  Created by mohammad takbiri on 11/15/20.
//

import Foundation
import UIKit
import CoreData

class DatabaseRequest {
    
    private init() {}
    static let shared = DatabaseRequest()
    
    
    
    func requestForSave(formulaStr: String, image: UIImage){
        
        guard let imageData = image.pngData() else {
            print("cant convert image to data")
            return
        }
        
        SaveInDB.save(formulaStr: formulaStr, imageData: imageData)
                
    }
    
    
    func requestForFetch(completion: @escaping ([[String:Any]])->Void){
        
        completion(FetchDB.fetch())
        
    }
    
}
