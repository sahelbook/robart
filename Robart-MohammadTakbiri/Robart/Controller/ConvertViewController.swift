//
//  HomeViewController.swift
//  Robart-MohammadTakbiri
//
//  Created by mohammad takbiri on 11/14/20.
//

import UIKit
import SCLAlertView
import SDWebImage
import SVProgressHUD

class ConvertViewController: UIViewController {
    
    @IBOutlet weak var formulaTextField: UITextField!
    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var formulaImageView: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
                
        configUI()
                
    }
    
    
    
    @IBAction func showButtonDidTouch(_ sender: Any) {
        
        if formulaTextField.text == "" || formulaTextField.text?.count ?? 0 < 2{
            
            SCLAlertView().showError("Message", subTitle: "Please enter a valid formula")
            
        }else {
            
            DatabaseRequest.shared.requestForFetch { (results) in
                if results.count != 0 {
                
                    var formulaSavedBedore:Bool = false
                    
                    results.forEach { (savedFormula) in
                        if (savedFormula["formula"] as! String) == self.formulaTextField.text {
                            
                            let imageData = savedFormula["image"] as! Data
                            self.formulaImageView.image = UIImage(data: imageData)
                            self.shareButton.alpha = 1.0
                            formulaSavedBedore = true
                            
                        }
                    }
                    
                    if formulaSavedBedore == false {
                        self.getFormulaFromWikiMedia()
                    }
                    
                }else {
                    self.getFormulaFromWikiMedia()
                }
            }
            
        }
        
    }
    
    @IBAction func shareButtonDidTouch(_ sender: Any) {
        self.formulaImageView.image.share()
    }
    
}

extension ConvertViewController {
    func configUI(){
        
        SVProgressHUD.setDefaultMaskType(.clear)
        
        formulaTextField.addBottomBorder()
        formulaTextField.changePlaceholderColor()
        
        showButton.layer.cornerRadius = showButton.frame.size.height/2
        showButton.layer.borderWidth = 1.0
        showButton.layer.borderColor = UIColor.white.cgColor
    }
    
    
    func getFormulaFromWikiMedia() {
        
        SVProgressHUD.show()
        
        APIsList.getHashCode(formula: formulaTextField.text!) { (response, success, headerFields) in
            
            SVProgressHUD.dismiss()
            
            if success {
                
                let resourceHeader = "\(headerFields["x-resource-location"]!)"
                guard let url = URL(string: "https://en.wikipedia.org/api/rest_v1/media/math/render/png/\(resourceHeader)") else {return}
                
                self.formulaImageView.sd_setImage(with: url) { (image, error, type, url) in
                    DatabaseRequest.shared.requestForSave(formulaStr: self.formulaTextField.text!, image: image!)
                }
                
                self.shareButton.alpha = 1.0
                
                
                
            }else {
                SCLAlertView().showError("Message", subTitle: "Something going wrong with your formula! Please check it and try again.")
            }
            
        }
        
    }
}
