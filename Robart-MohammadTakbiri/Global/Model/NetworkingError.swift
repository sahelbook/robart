//
//  NetworkingError.swift
//  Peyk-Swift
//
//  Created by mohammad takbiri on 11/4/19.
//  Copyright © 2019 mohammad takbiri. All rights reserved.
//

import UIKit
import SCLAlertView

class NetworkingError {
    
    static func showNetworkError() {
        
        SCLAlertView().showError("Message", subTitle: "Something wrong bad with your internet connection. \n Please check it and try again.")
        
    }
    
}
