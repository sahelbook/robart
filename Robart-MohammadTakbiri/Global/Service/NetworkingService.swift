//
//  NetworkingService.swift
//  Peyker-Swift
//
//  Created by mohammad takbiri on 9/1/20.
//  Copyright © 2020 mohammad takbiri. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

class NetworkingService {
    
    private init() {}
    static let shared = NetworkingService()
    
    func callAPI(fromURL url:URL, withParamter parameter:[String: Any], completion: @escaping (Any, [AnyHashable:Any])-> Void){
        
        SVProgressHUD.setDefaultMaskType(.clear)
        
        
        print("link is :\(url) and parameter is :\(parameter)")
        AF.request(url, method: .post, parameters: parameter).responseJSON { response in
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            
            
            switch response.result{
            case .success:
                
                DispatchQueue.main.async {
                    
                    guard let result = response.value else { return }
                    completion(result, response.response?.allHeaderFields ?? ["":""])
                }
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    
                    NetworkingError.showNetworkError()
                }

                print("error for receiveing network is :\(error.localizedDescription)")

                break

            }

        }
    }

    
}
